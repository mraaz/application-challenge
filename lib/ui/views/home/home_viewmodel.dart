import 'package:application_challenge/app/app.locator.dart';
import 'package:application_challenge/core/api_service.dart';
import 'package:stacked/stacked.dart';
import 'home_view.form.dart';

class HomeViewModel extends FormViewModel {
  final _weatherApi = locator<ApiService>();

  double? _temperature;
  double? get temperature => _temperature;

  //TODO: Der Api Request enthält noch einen Fehler, weil er als Parameter die Stadt des TextFields aus der HomeView benötigt
  //Der Wert kann über den Getter [cityValue] übergeben werden
  Future<void> getWeatherData() async {
    final result = await _weatherApi.getWeatherDataByCity();
    _temperature = result;
    //TODO: Das Ergebnis wird in Fahrenheit zurückgegeben. Führe hier convertResult() aus, die das Ergebnis in Grad Celcius umwandelt.
    notifyListeners();
  }

  void convertResult() {
    //TODO: Konvertiere das Ergebnis hier
    notifyListeners();
  }

  @override
  void setFormStatus() {}
}
