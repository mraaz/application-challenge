import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import 'package:application_challenge/ui/common/app_colors.dart';
import 'package:application_challenge/ui/common/ui_helpers.dart';
import 'package:stacked/stacked_annotations.dart';
import 'home_view.form.dart';
import 'home_viewmodel.dart';

@FormView(fields: [
  FormTextField(
    name: 'city',
  ),
])
class HomeView extends StatelessWidget with $HomeView {
  HomeView({super.key});

  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<HomeViewModel>.reactive(
      viewModelBuilder: () => HomeViewModel(),
      onModelReady: listenToFormUpdated,
      builder: (context, model, child) => Scaffold(
        body: SafeArea(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 25.0),
            child: Center(
              child: Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  verticalSpaceLarge,
                  Column(
                    children: [
                      const Text(
                        'Hello! 👋',
                        style: TextStyle(
                          fontSize: 35,
                          fontWeight: FontWeight.w900,
                        ),
                      ),
                      verticalSpaceMedium,
                      const Text(
                        'Zeig mir das Wetter in',
                        style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.w900,
                        ),
                      ),
                      verticalSpaceMedium,
                      //TODO: Baue hier ein TextFormField() ein für die Eingabe der Stadt.
                      // Du musst dem TextFormField einen Controller übergeben, das kannst du über den Getter [cityController]
                      // Als zweites soll dem Parameter onFieldSubmitted eine Funktion übergeben werden aus dem ViewModel. 
                      // Das kannst du über [model.functionName] machen
                      verticalSpaceMedium,
                      if (model.temperature != null)
                        Text(
                          'Aktuell sind es ${model.temperature.toString()} Grad.',
                          style: const TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.w900,
                          ),
                        ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      //TODO: Implementier hier einen MaterialButton, der wie auf dem Screenshot aussieht. Tipp: Die Farbe is kcDarkGreyColor
                      //Übergib dem Parameter onPressed wieder die Funktion aus dem ViewModel
                    ],
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
