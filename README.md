# Deep5 Flutter Challenge

Diese Flutter-App stellt eine Programmierherausforderung für Flutter-Enthusiasten dar, die sich für eine Stelle bei [Deep5](https://www.deep5.io) (https://www.deep5.io) bewerben. 

Wenn du aus irgendeinem anderen Grund auf dieses Repository gestoßen bist, kannst du gerne an der Herausforderung teilnehmen, um deine Langeweile zu vertreiben. Aber stelle sicher, dass du uns nach Beendigung eine Bewerbung schickst 💪🏻

## Anforderungen

Die App wurde entwickelt mit `flutter 3.3.5` (`dart 2.18.2`). Wenn Flutter noch nicht bereits installiert ist, bitte an der Dokumentation (https://docs.flutter.dev) entlang hangeln.

## Assignment

In dieser Programmieraufgabe wirst du eine Anwendung implementieren, die eine Eingabe vom Benutzer annimmt und die Eingabe an eine Wetter API sendet, um die aktuelle Temperatur des Standortes, in Celsius, anzuzeigen. Der Fluss des Benutzers ist der folgende:

1. Der Benutzer wird auf dem Startbildschirm, der HomeView() begrüßt, das ein "TextFormField()" und einen Button für die Eingabe benötigt. 
1. Der Benutzer gibt einen Namen einer Stadt in das Textfeld ein und drückt auf den Button, um die Anfrage an die Wetter API zu senden.
1. Die Anwendung berechnet die Anfrage, während die Benutzeroberfläche auf die Antwort wartet.
1. Sobald die Anfrage abgeschlossen ist, wird die Antwortmeldung in der Benutzeroberfläche oberhalb des Buttons "Suchen" angezeigt. Wenn bei der Anforderung vom Server ein Fehler auftritt, wird stattdessen eine Fehlermeldung angezeigt.

## Aufgaben

1. Die Dokumentation lesen.
1. Clone das Repository.
1. Verstehe den bereitgestellten Code.
1. Integriere ein `TextFormField`, welches den Namen einer Stadt akzeptiert.
    - Vergewissere dich, dass keine ungültigen Eingaben (z. B. Zahlen) eingegeben wurden!
1. Integriere einen `MaterialButton`.
1. Implementiere die Verarbeitung der Anfrage, wenn der Button "onPressed" ausgelöst wird.
1. Konvertiere das Ergebnise der Anfrage von Fahrenheit in Grad Celsius.
1. Anzeigen der Antwort des HTTP Requests.
1. Lade deinen Code in ein neues Repository hoch 
    - keine Abzweigung erzeugen
    - keinen pull request erstellen
1. [Sende uns den Link](office@deep5.io) (office@deep5.io) zu deinem Repository, mit deiner entsprechenden Lösung der Problematik.
    - wenn du nicht möchtest, dass deine Lösung öffentlich zugänglich ist, erstelle ein privates Repository und lade [Matthias Raaz](matthias.raaz@deep5.io) (matthias.raaz@deep5.io) ein.
1. So sieht die App aus, wenn du fertig mit den Aufgaben bist. 
![Alt text](application_screenshot.png "Application Challenge")

## Zusätzliche Informationen
- Du darfst jedes auf [pub.dev](https://pub.dev) (https://pub.dev) verfügbare Paket verwenden.
- Du darfst zusätzliche `Widget`s und Klassen erstellen.
- Wir erwarten kein ausgefallenes Design. Für UI-Komponenten kannst du Material- oder Cupertino-Widgets verwenden. 
- Stelle sicher, dass du eine Lösung einreichst, von der du sagen kannst, dass sie Standard-Softwareentwicklungsmuster und praktiken umfasst. 

## Fragen?

- Wenn du Fragen zu dieser Aufgabe hast, lass es uns in wissen!

## Have fun! 

